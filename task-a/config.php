<?php
class Config
{
	var $host = "localhost";
	var $uname = "root";
	var $pass = "";
	var $db = "task_a";
	var $conn = null;

	function __construct()
	{

		$this->conn = new mysqli($this->host, $this->uname, $this->pass, $this->db);
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
	}

	function validate($input)
	{
		$reserved_word = array('TAHUN', 'THN', 'TH',);
		$main_input = str_replace($reserved_word, "", strtoupper($input));
		$age = str_replace(' ', '', (int)filter_var($main_input, FILTER_SANITIZE_NUMBER_INT));
		$name = strtok($main_input, $age);
		$city = str_replace($age, " ", strstr($main_input, $age));
		$this->store($name, $age, $city);
	}

	function store($name, $age, $city)
	{
		$sql = "INSERT INTO person (name, age, city)
				VALUES ('$name', '$age', '$city')";
		if ($this->conn->query($sql) === true) {
			echo "New record created successfully";
		} else {
			echo "Error: " . $sql . "<br>" . $conn->error;
		}
	}

	function showData()
	{
		$sql = "SELECT * FROM person ORDER BY id DESC";
		$result = $this->conn->query($sql);
		return $result;
	}
}

?>