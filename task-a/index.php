<!DOCTYPE html>
<html lang="en">
<?php
include 'config.php';
$config = new config();

?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Input</title>
</head>
<body>
    <form action="input.php" method="post">
        <input type="text" name="main_input">
        <button type="submit">Submit</button>
    </form>
    <br>
    <table border="2px;">
        <tr>
            <th>Name</th>
            <th>Age</th>
            <th>City</th>
        </tr>
        <?php foreach ($config->showData() as $data) : ?>
            <tr>
                <td><?php echo $data['name']; ?></td>
                <td><?php echo $data['age']; ?></td>
                <td><?php echo $data['city']; ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
</body>
</html>