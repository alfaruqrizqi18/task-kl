<?php
$file_lines = file('access.log');
$output = array();
foreach ($file_lines as $log) {
    preg_match('/\[(.+)\]/', $log, $data);
    $_all_date = $data[1]; // example : [25/Aug/2017:16:56:10 +0700]
    $_date_spliting = explode(":", $_all_date);
    $tmp_date = explode("/", $_date_spliting[0]);
    $month_names = array(
        '01' => 'Jan',
        '02' => 'Feb',
        '03' => 'Mar',
        '04' => 'Apr',
        '05' => 'May',
        '06' => 'Jun',
        '07' => 'Jul',
        '08' => 'Aug',
        '09' => 'Sep',
        '10' => 'Oct',
        '11' => 'Nov',
        '12' => 'Dec',
    );

    $day = $tmp_date[0];
    $month = array_search($tmp_date[1], $month_names);
    $year = $tmp_date[2];
    $final_date = $year . '-' . $month . '-' . $day;

//get address
    preg_match('/\"GET (.[^\"]+)/', $log, $data_url);
    $tmp_web_address = explode(" ", $data_url[1]);
    $web_address = $tmp_web_address[0];
    $http = substr($tmp_web_address[1], -1);
    $final_web_address = $web_address . ' ' . $http;

    $final_output = $final_date . ' ' . $final_web_address;
    array_push($output, $final_output);;
}
print_r($output);
echo '<a href="http://localhost/task-kl/task-d/">Kembali</a>'
?>